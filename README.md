# Bengali Word List
#### A Large Collection of Bengali Words

### Description
BengaliWordList\_48.txt, BengaliWordList\_112.txt, & BengaliWordList\_439.txt contains more than 48000, 112000, & 439000 Bengali words respectively. These lists can be used for spell checking and many more other purposes. The file is encoded in UTF-8 & words are separated with new line. Shorter list contains words only of higher frequancy.

<div align="center">
  <img src="https://cloud.githubusercontent.com/assets/5456665/19994067/c1d80750-a274-11e6-8160-2af151f5e966.png" height="400" width=auto title="bengali word list preview" />
</div>
 
### Download
[BengaliWordList\_48](https://github.com/MinhasKamal/BengaliWordList/raw/download/BengaliWordList_48.rar) <br />
[BengaliWordList\_112](https://github.com/MinhasKamal/BengaliWordList/raw/download/BengaliWordList_112.rar) <br />
[BengaliWordList\_439](https://github.com/MinhasKamal/BengaliWordList/raw/download/BengaliWordList_439.rar)

### License
GPL-3.0
